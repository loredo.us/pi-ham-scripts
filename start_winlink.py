#!/usr/bin/env python3


import subprocess
import argparse
import signal
import sys
import time

CONTINUE = True

### CONSTANT CONFIG
DEFAULT_ARDOPC_INPUT = 'pulse'
DEFAULT_ARDOPC_OUTPUT = 'pulse'
DEFAULT_RIGCTL_MODEL = 376
DEFAULT_RIGCTL_PORT = '/dev/ttyUSB0'



def parse_arguments():
    parser = argparse.ArgumentParser()
    # Ardop arguments
    parser.add_argument('-ab','--ardop-bin',default='/usr/local/bin/piardopc',help='Path to ardopc binary')
    parser.add_argument('-ap','--ardop-port',type=int,default=8515,help='Port number to use for ardopc')
    parser.add_argument('-ai','--ardop-in',default=DEFAULT_ARDOPC_INPUT,help='Input device to use for ardopc')
    parser.add_argument('-ao','--ardop-out',default=DEFAULT_ARDOPC_OUTPUT,help='Output device to use for ardopc')
    # Rigctld arguments
    parser.add_argument('-rm','--rigctl-model',type=int,default=DEFAULT_RIGCTL_MODEL,help='Rigctl model number to use')
    parser.add_argument('-rp','--rigctl-port',default=DEFAULT_RIGCTL_PORT,help='Serial port to use for Rigctl')
    parser.add_argument('-rb','--rigctl-bin',default='/usr/bin/rigctld',help='Path to rigctld binary')
    # Pat arguments
    parser.add_argument('-pb','--pat-bin',default='/usr/bin/pat',help='Path to pat binary')

    # Script arguments
    parser.add_argument('-w','--watchdog',action='store_true',help='Should the script act as a watchdog and restart failing components?')
    
    return parser.parse_args()

def main_body():
    global CONTINUE 
    CONTINUE = True
    signal.signal(signal.SIGINT, handle_sigint)
    conf = parse_arguments()
    ardop_process = start_ardop(conf)
    time.sleep(1)
    rigctld_process = start_rigctld(conf)
    time.sleep(1)
    pat_process = start_pat(conf)
    time.sleep(1)

    while CONTINUE:
        if ardop_process.poll():
            print(f'***ARDOPC Died with code "{str(ardop_process.poll())}"***')
            if conf.watchdog:
                print('***Restarting ARDOPC***')
                ardop_process = start_ardop(conf)
            else:
                CONTINUE = False
        if rigctld_process.poll():
            print(f'***RIGCTLD Died with code "{str(rigctld_process.poll())}"')
            if conf.watchdog:
                print('***Restarting RIGCTLD***')
                rigctld_process = start_rigctld(conf)
            else:
                CONTINUE = False
        if pat_process.poll():
            print(f'***PAT Died with code "{str(pat_process.poll())}"')
            if conf.watchdog:
                print('***Restarting PAT***')
                pat_process = start_pat(conf)
            else:
                CONTINUE = False
        time.sleep(5)
    
    print('***Shutting down***')
    print('***Killing ARDOP***')
    ardop_process.kill()
    print('***Killing RIGCTLD***')
    rigctld_process.kill()
    print('***Killing PAT***')
    pat_process.kill()
    print('***Ending***')
    


def start_ardop(conf):
    print('***Starting Ardopc subprocess***')
    process_def = [conf.ardop_bin, str(conf.ardop_port), conf.ardop_in, conf.ardop_out]
    return subprocess.Popen(process_def)

def start_rigctld(conf):
    print('***Starting rigctld subprocess***')
    process_def = [conf.rigctl_bin,'-m',str(conf.rigctl_model),'-r',conf.rigctl_port,'-vvv']
    return subprocess.Popen(process_def)
    
def start_pat(conf):
    print('***Starting pat subprocess***')
    process_def = [conf.pat_bin,'http']
    return subprocess.Popen(process_def)
    
def handle_sigint(sig, frame):
    print('***SIGINT Received***')
    global CONTINUE 
    CONTINUE = False

if __name__ == "__main__":
    main_body()